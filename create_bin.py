#
#
# Find all executables (except scripts): find . -type f -executable -exec basename {} \; | grep -v "\.py" | grep -v "\.pl" | grep -v "\.rb" | grep -v "\.lua"
#
# Find all symlink: find . -type l -executable -exec basename {} \;
#
#
import os
save_dir = "andraxbin"
progpath = "/opt/ANDRAX/john"

extra_bins = [ "hccap2john",
"racf2john",
"cprepair",
"mailer",
"bitlocker2john",
"uaf2john",
"calc_stat",
"genmkvpwd",
"wpapcap2john",
"putty2john",
"tgtsnarf",
"SIPdump",
"john",
"makechr",
"raw2dyna",
"keepass2john",
"eapmd5tojohn",
"dmg2john",
"vncpcap2john",
"mkvcalcproba",
"relbench",
"benchmark-unify",
"zip2john",
"unshadow",
"unique",
"unafs",
"gpg2john",
"rar2john",
"undrop",
"base64conv" ]

str_pl_file = """#!/bin/bash

perl {progdir}/{progname} "$@"
"""

str_rb_file = """#!/bin/bash

ruby {progdir}/{progname} "$@"
"""

str_py_file = """#!/bin/bash

source /opt/ANDRAX/PYENV/python3/bin/activate

/opt/ANDRAX/PYENV/python3/bin/python3 {progdir}/{progname} "$@"

"""

str_extra_file = """#!/bin/bash

{progdir}/{progname} "$@"
"""

#os.system("rm -rf "+save_dir)
os.system("mkdir "+save_dir)

if os.path.exists("run"):

    exfiles = os.listdir("run")
    
    for exfile in exfiles:

        if exfile.endswith('.py'):
            with open(save_dir+"/"+exfile, 'w') as out:
                content = str_py_file.format(progdir = progpath, progname = exfile)
                out.write(content)

        elif exfile.endswith('.pl'):
            with open(save_dir+"/"+exfile, 'w') as out:
                content = str_pl_file.format(progdir = progpath, progname = exfile)
                out.write(content)

        elif exfile.endswith('.rb'):
            with open(save_dir+"/"+exfile, 'w') as out:
                content = str_rb_file.format(progdir = progpath, progname = exfile)
                out.write(content)

else:
    print("run dir not found")

for x in extra_bins:
  with open(save_dir+"/"+x, 'w') as out:
      content = str_extra_file.format(progdir = progpath, progname = x)
      out.write(content)
